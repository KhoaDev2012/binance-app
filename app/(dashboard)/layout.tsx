import Header from "@/components/header";

interface DashboardLayoutProps {
  children: React.ReactNode;
}

const DashboardLayout = ({ children }: DashboardLayoutProps) => {
  return (
    <>
      <Header />
      <main className="px-4 lg:px-16">{children}</main>
    </>
  );
};

export default DashboardLayout;
