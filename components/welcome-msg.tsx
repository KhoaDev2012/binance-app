"use client";

import { useUser } from "@clerk/nextjs";

const WelcomeMsg = () => {
  const { user, isLoaded } = useUser();

  return (
    <div className="space-y-2 mb-4">
      <h2 className="text-2xl lg:text-4xl text-white font-medium ">
        Welcome back{isLoaded ? ", " : " "}
        {user?.firstName?.toUpperCase()} ✌️
      </h2>
      <p className="text-sm lg:text-base text-orange-200">
        This is your Finance Overview Report
      </p>
    </div>
  );
};

export default WelcomeMsg;
